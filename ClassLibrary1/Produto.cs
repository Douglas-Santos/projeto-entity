﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model.Conexao;

namespace Model
{
    [Table("produto", Schema = "public")]
    public class Produto
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("nome")]
        public string NomeProduto { get; set; }

        [Column("data_cadastro")]
        public DateTime DataCadastro { get; set; }

        public void SaveOrCreate()
        {
            try
            {
                using (var conexao = new Conexao.Conexao())
                {
                    conexao.Produto.Add(this);
                    conexao.Entry(this).State = Id > 0 ? EntityState.Modified : EntityState.Added;
                    conexao.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Produto ConsultarProduto(int Id, string nome = null)
        {
            Produto p;

            using (var conexao = new Conexao.Conexao())
            {
                //sql utilizando expressão lambda
                var query = from Prod in conexao.Produto
                            where Prod.Id == Id
                            select Prod;
                p = query as Produto;
            }
            return p;
        }

        public BindingSource ConsultarProdutos(int? Id, string Nome)
        {
            BindingSource bsProdutos = new BindingSource();
            using (var conexao = new Conexao.Conexao())
            {
                var bindingProduto = from Prod in conexao.Produto
                                     where (Prod.Id == Id || Id == null) &&
                                           (Prod.NomeProduto.Contains(Nome.ToLower()) || string.IsNullOrEmpty(Nome ))
                                     select Prod;
                bsProdutos.DataSource = bindingProduto.ToList();
            }
            
            return bsProdutos;
        }
    }
}
