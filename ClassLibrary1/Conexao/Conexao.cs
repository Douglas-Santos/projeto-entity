﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Conexao
{
    public class Conexao : DbContext
    {
        public Conexao() : base("Connect") { }

        public DbSet<Produto> Produto { get; set; }
    }
}
