﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2;

namespace Aplicacao
{
    public partial class PesquisaProduto : Form
    {
        public PesquisaProduto()
        {
            InitializeComponent();
        }

        public void CadastrarProduto()
        {
            var cadastro = new CadastroProduto();
            cadastro.StartPosition = FormStartPosition.CenterParent;
            if(cadastro.ShowDialog() == DialogResult.OK)
            {
                cadastro.Close();
                ConsultarProdutos(null, Nome.Text);
            }
        }

        public void ConsultarProdutos(int? Id, string Nome)
        {
            var produto = new Produto();
            int _id = 0;
            if (Id > 0)
                _id = Convert.ToInt16(Id);

            if (_id == 0)
                bindingSourceProduto = produto.ConsultarProdutos(null, Nome);
            else
                bindingSourceProduto = produto.ConsultarProdutos(_id, Nome);

            dataGridView1.DataSource = bindingSourceProduto;
        }

        private void Codigo_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Codigo_Leave(object sender, EventArgs e)
        {
           
        }

        private void Nome_Leave(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Codigo.Text) && !string.IsNullOrEmpty(Nome.Text))
                ConsultarProdutos(Id: Convert.ToInt16(Codigo.Text), Nome: Nome.Text);

            else if (!string.IsNullOrEmpty(Codigo.Text) && string.IsNullOrEmpty(Nome.Text))
                ConsultarProdutos(Id: Convert.ToInt16(Codigo.Text), Nome: Nome.Text);

            else if (string.IsNullOrEmpty(Codigo.Text) && !string.IsNullOrEmpty(Nome.Text))
                ConsultarProdutos(Id: null, Nome: Nome.Text);

            else
                ConsultarProdutos(Id: null, Nome: string.Empty);
        }

        private void BtnCadastrar_Click(object sender, EventArgs e)
        {
            CadastrarProduto();
        }
    }
}
