﻿namespace WindowsFormsApp2
{
    partial class CadastroProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastroProduto));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.BotaoSalvar = new System.Windows.Forms.ToolStripButton();
            this.BotaoCancelar = new System.Windows.Forms.ToolStripButton();
            this.BtnConsultar = new System.Windows.Forms.ToolStripButton();
            this.dataCadastro = new System.Windows.Forms.MaskedTextBox();
            this.nomeProduto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Codigo = new System.Windows.Forms.TextBox();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.toolStrip1);
            this.groupBox1.Controls.Add(this.dataCadastro);
            this.groupBox1.Controls.Add(this.nomeProduto);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Codigo);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(631, 104);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cadastro de produtos";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BotaoSalvar,
            this.BotaoCancelar,
            this.BtnConsultar});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(3, 76);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(625, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // BotaoSalvar
            // 
            this.BotaoSalvar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.BotaoSalvar.Image = ((System.Drawing.Image)(resources.GetObject("BotaoSalvar.Image")));
            this.BotaoSalvar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BotaoSalvar.Name = "BotaoSalvar";
            this.BotaoSalvar.Size = new System.Drawing.Size(58, 22);
            this.BotaoSalvar.Text = "Salvar";
            this.BotaoSalvar.Click += new System.EventHandler(this.BotaoSalvar_Click);
            // 
            // BotaoCancelar
            // 
            this.BotaoCancelar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.BotaoCancelar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BotaoCancelar.Name = "BotaoCancelar";
            this.BotaoCancelar.Size = new System.Drawing.Size(57, 22);
            this.BotaoCancelar.Text = "Cancelar";
            this.BotaoCancelar.Click += new System.EventHandler(this.BotaoCancelar_Click);
            // 
            // BtnConsultar
            // 
            this.BtnConsultar.Image = ((System.Drawing.Image)(resources.GetObject("BtnConsultar.Image")));
            this.BtnConsultar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtnConsultar.Name = "BtnConsultar";
            this.BtnConsultar.Size = new System.Drawing.Size(124, 22);
            this.BtnConsultar.Text = "Consultar produto";
            this.BtnConsultar.Click += new System.EventHandler(this.BtnConsultar_Click);
            // 
            // dataCadastro
            // 
            this.dataCadastro.Location = new System.Drawing.Point(502, 19);
            this.dataCadastro.Mask = "00/00/0000";
            this.dataCadastro.Name = "dataCadastro";
            this.dataCadastro.Size = new System.Drawing.Size(72, 20);
            this.dataCadastro.TabIndex = 2;
            this.dataCadastro.ValidatingType = typeof(System.DateTime);
            // 
            // nomeProduto
            // 
            this.nomeProduto.Location = new System.Drawing.Point(120, 19);
            this.nomeProduto.Name = "nomeProduto";
            this.nomeProduto.Size = new System.Drawing.Size(376, 20);
            this.nomeProduto.TabIndex = 1;
            this.nomeProduto.TextChanged += new System.EventHandler(this.nomeProduto_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Código";
            // 
            // Codigo
            // 
            this.Codigo.Location = new System.Drawing.Point(51, 19);
            this.Codigo.Name = "Codigo";
            this.Codigo.Size = new System.Drawing.Size(63, 20);
            this.Codigo.TabIndex = 0;
            this.Codigo.TextChanged += new System.EventHandler(this.Codigo_TextChanged);
            this.Codigo.Leave += new System.EventHandler(this.Codigo_Leave);
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // CadastroProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 104);
            this.Controls.Add(this.groupBox1);
            this.Name = "CadastroProduto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de produtos";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Codigo;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton BotaoSalvar;
        private System.Windows.Forms.ToolStripButton BotaoCancelar;
        private System.Windows.Forms.MaskedTextBox dataCadastro;
        private System.Windows.Forms.TextBox nomeProduto;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.ToolStripButton BtnConsultar;
    }
}

