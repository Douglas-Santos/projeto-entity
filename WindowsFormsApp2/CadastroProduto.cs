﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;


namespace WindowsFormsApp2
{
    public partial class CadastroProduto : Form
    {
        private Produto produto = new Produto();

        public CadastroProduto()
        {
            InitializeComponent();
        }

        public bool ValidarCampos()
        {
            bool valido = true;

            if (string.IsNullOrEmpty(nomeProduto.Text))
            {
                errorProvider.SetError(nomeProduto, "O nome do produto é obrigatório");
                valido = false;
            }

            return valido;
        }

        public void SalvarProduto()
        {
            if (ValidarCampos())
            {
                try
                {
                    produto = new Produto();
                    produto.NomeProduto = nomeProduto.Text;
                    produto.DataCadastro = !string.IsNullOrEmpty(dataCadastro.Text) ?
                                            Convert.ToDateTime(dataCadastro.Text) :
                                            DateTime.MinValue;

                    produto.SaveOrCreate();

                    Codigo.Text = produto.Id.ToString();

                    var mensagem = string.Format("Produto {0}-{1} cadastrado com sucesso!", produto.Id, produto.NomeProduto);

                    MessageBox.Show(this, mensagem, "Produto cadastrado", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Codigo.Enabled = false;
                    this.DialogResult = DialogResult.OK;
                    this.Close();

                }catch(Exception ex)
                {
                    var mensagem = string.Format("Ocorreu um erro ao gravar o produto Erro: {0}.", ex.Message);
                    MessageBox.Show(this, mensagem, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }

        }

        public void ConsultarProduto(int Id)
        {
            var prod = produto ?? new Produto();

            produto = prod.ConsultarProduto(Id);
            if(produto != null)
            {
                nomeProduto.Text = produto.NomeProduto;
                dataCadastro.Text = produto.DataCadastro.ToShortDateString();
            }
        }
        
        private void Codigo_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Codigo.Text))
                ConsultarProduto(Convert.ToInt16(Codigo.Text));
        }

        private void BotaoSalvar_Click(object sender, EventArgs e)
        {
            SalvarProduto();
        }

        private void Codigo_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Codigo.Text))
                errorProvider.Clear();
        }

        private void nomeProduto_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(nomeProduto.Text))
                errorProvider.Clear();
        }

        private void BtnConsultar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Codigo.Text))
            {
                var id = Convert.ToInt16(Codigo.Text);
                ConsultarProduto(id);
            }
        }

        private void BotaoCancelar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Deseja encerrar o progarama?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                this.Close();
        }
    }
}
